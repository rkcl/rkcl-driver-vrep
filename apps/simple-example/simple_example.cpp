/*      File: main.cpp
 *       This file is part of the program rkcl-controller
 *       Program description : Asetofclassesfordual-armcontrolusingthecooperativetaskspacerepresentation
 *       Copyright (C) 2018 -  Sonny Tarbouriech (LIRMM / Tecnalia) Benjamin Navarro (LIRMM). All Right reserved.
 *
 *       This software is free software: you can redistribute it and/or modify
 *       it under the terms of the LGPL license as published by
 *       the Free Software Foundation, either version 3
 *       of the License, or (at your option) any later version.
 *       This software is distributed in the hope that it will be useful,
 *       but WITHOUT ANY WARRANTY without even the implied warranty of
 *       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *       LGPL License for more details.
 *
 *       You should have received a copy of the GNU Lesser General Public License version 3 and the
 *       General Public License version 3 along with this program.
 *       If not, see <http://www.gnu.org/licenses/>.
 */
#include <rkcl/drivers/vrep_driver.h>
#include <rkcl/processors/vrep_visualization.h>
#include <rkcl/processors/otg_reflexxes.h>
#include <rkcl/processors/app_utility.h>
#include <rkcl/processors/forward_kinematics_rbdyn.h>
#include <rkcl/processors/osqp_solver.h>
#include <pid/signal_manager.h>
#include <rkcl/processors/internal/internal_functions.h>

#include <yaml-cpp/yaml.h>
#include <pid/rpath.h>
#include <iostream>

int main(int argc, char* argv[])
{
    rkcl::DriverFactory::add<rkcl::VREPMainDriver>("vrep_main");
    rkcl::DriverFactory::add<rkcl::VREPJointDriver>("vrep_joint");
    rkcl::QPSolverFactory::add<rkcl::OSQPSolver>("osqp");

    auto conf = YAML::LoadFile(PID_PATH("examples_config/kuka_lwr4_config.yaml"));
    auto app = rkcl::AppUtility::create<rkcl::ForwardKinematicsRBDyn, rkcl::JointSpaceOTGReflexxes>(conf);

    rkcl::TaskSpaceOTGReflexxes task_space_otg(app.robot(), app.taskSpaceController().controlTimeStep());

    if (not app.init())
    {
        throw std::runtime_error("Cannot initialize the application");
    }

    rkcl::VREPVisualization vrep_visu(app.robot());
    vrep_visu.init();

    app.addDefaultLogging();

    bool stop = false;
    bool done = false;

    pid::SignalManager::registerCallback(pid::SignalManager::Interrupt, "stop",
                                         [&](int) { stop = true; });

    try
    {
        std::cout << "Starting control loop \n";
        app.configureTask(0);
        task_space_otg.reset();
        while (not stop and not done)
        {
            bool ok = app.runControlLoop(
                [&] {
                    if (app.isTaskSpaceControlEnabled())
                        return task_space_otg();
                    else
                        return true;
                },
                [&] {
                    if (app.isTaskSpaceControlEnabled())
                        return vrep_visu();
                    else
                        return true;
                });

            if (ok)
            {
                done = true;
                if (app.isTaskSpaceControlEnabled())
                {
                    double error_norm = 0;
                    for (size_t i = 0; i < app.robot().controlPointCount(); ++i)
                    {
                        auto cp_ptr = std::static_pointer_cast<rkcl::ControlPoint>(app.robot().controlPoint(i));
                        if (cp_ptr->isEnabled())
                        {
                            auto error = rkcl::internal::computePoseError(cp_ptr->goal().pose(), cp_ptr->state().pose());
                            error_norm += (static_cast<Eigen::Matrix<double, 6, 6>>(cp_ptr->selectionMatrix().positionControl()) * error).norm();
                            std::cout << "error norm = " << error_norm << "\n";
                        }
                    }

                    done &= (error_norm < 0.01);
                }
                if (app.isJointSpaceControlEnabled())
                {
                    for (const auto& joint_space_otg : app.jointSpaceOTGs())
                    {
                        if (joint_space_otg->jointGroup()->controlSpace() == rkcl::JointGroup::ControlSpace::JointSpace)
                        {
                            if (joint_space_otg->controlMode() == rkcl::JointSpaceOTG::ControlMode::Position)
                            {
                                auto joint_group_error_pos_goal = joint_space_otg->jointGroup()->selectionMatrix().value() * (joint_space_otg->jointGroup()->goal().position() - joint_space_otg->jointGroup()->state().position());
                                done &= (joint_group_error_pos_goal.norm() < 0.001);
                            }
                            else if (joint_space_otg->controlMode() == rkcl::JointSpaceOTG::ControlMode::Velocity)
                            {
                                auto joint_group_error_vel_goal = joint_space_otg->jointGroup()->selectionMatrix().value() * (joint_space_otg->jointGroup()->goal().velocity() - joint_space_otg->jointGroup()->state().velocity());
                                done &= (joint_group_error_vel_goal.norm() < 1e-10);
                            }
                        }
                    }
                }
            }
            else
            {
                throw std::runtime_error("Something wrong happened in the control loop, aborting");
            }
            if (done)
            {
                done = false;
                std::cout << "Task completed, moving to the next one" << std::endl;
                done = not app.nextTask();
                task_space_otg.reset();
            }
        }
        if (stop)
            throw std::runtime_error("Caught user interruption, aborting");

        std::cout << "All tasks completed" << std::endl;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    pid::SignalManager::unregisterCallback(pid::SignalManager::Interrupt, "stop");

    app.end();
}