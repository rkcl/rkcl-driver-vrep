/**
 * @file vrep_main_driver.h
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a V-REP main driver to manage the execution of tasks on simulation
 * @date 21-02-2020
 * License: CeCILL
 */

#pragma once

#include <rkcl/drivers/driver.h>
#include <string>
#include <vector>
#include <atomic>

#include <condition_variable>

namespace rkcl
{

/**
 * @brief Declare the existence of the class VREPJointDriver which manages the control of individual joint groups
 *
 */
class VREPJointDriver;

/**
 * @brief Define a class to manage the V-REP simulation of robot operations
 */
class VREPMainDriver : virtual public Driver
{
public:
    /**
	 * @brief Structure holding synchronization data
	 *
	 */
    struct SyncData
    {
        std::mutex mtx;                   //!< Mutex to prevent concurrent access and modifications to the sync data
        std::condition_variable cv;       //!<Condition variable used to notify other drivers
        std::vector<bool> notify_drivers; //!< Vector whose indices are associated to joint driver indices, indicate the corresponding driver has to be notified
        bool stop;                        //!< Indicate whether the simulation has to be stopped or not
    };

    /**
	 * @brief Construct a new VREPMainDriver object with parameters
	 * @param cycle_time Simulator control time step
	 * @param port Number of port used to communicate with the simulator (default 19997)
	 * @param ip IP address to communicate with the simulator (default 127.0.0.1)
	 */
    VREPMainDriver(double cycle_time,
                   int port = 19997,
                   const std::string& ip = "127.0.0.1");

    /**
	 * @brief Construct a new VREPMainDriver object from a YAML configuration file
	 * Accepted values are :
	 * - 'cycle_time' (double): Simulator control time step
	 * - 'port' (int):  Number of port used to communicate with the simulator (default 19997)
	 * - 'ip' (string): IP address to communicate with the simulator (default 127.0.0.1)
	 * @param robot a reference to the shared robot
	 * @param configuration a YAML node containing the configuration of the driver
	 */
    VREPMainDriver(
        Robot& robot,
        const YAML::Node& configuration);

    /**
	 * @brief Destroy the VREPMainDriver object
	 *
	 */
    virtual ~VREPMainDriver();

    /**
	 * @brief Not used for this driver
	 * @param timeout unused
	 * @return true
	 */
    virtual bool init(double timeout = 30.) override;

    /**
	 * @brief Start the V-REP simulation.
	 * @return true if the driver was able to run a simulation step, false otherwise
	 */
    virtual bool start() override;

    /**
	* @brief Stop the simulation.
	* @return true if the driver was able to stop the V-REP simulator, false otherwise
	*/
    virtual bool stop() override;

    /**
	 * @brief Do nothing as this driver is not intended to control a joint group
	 * @return true
	 */
    virtual bool read() override;

    /**
	 * @brief Do nothing as this driver is not intended to control a joint group
	 * @return true
	 */
    virtual bool send() override;

    /**
	 * @brief Wait for the current step to finish, send a synchronization trigger signal to the server
	 * and notify the joint group drivers that new data are available
	 * @return true if the trigger signal was properly executed, false otherwise
	 */
    virtual bool sync() override;

    /**
	 * @brief Update the value of the real time factor considering the
	 * actual time spent to process one simulation step
	 */
    void updateRealTimeFactor();

    /**
	 * @brief Sleep until the control step ends
	 *
	 */
    void waitNextCycle();
    /**
	 * @brief Notify all the joint drivers that the new state has been
	 * processed on simulation
	 */
    void notifyNewData();

    /**
	 * @brief Notify all the joint drivers that the simulation is ending
	 *
	 */
    static void notifyStop();

    static SyncData sync_data;      //!< Static variable used for the communication between main/joint drivers
    static int32_t getClientID();   //!< Static variable used to access the client ID provided by the V-REP simulator
    static std::mutex vrep_com_mtx; //!< Mutex used to avoid concurrent communication with V-REP

private:
    friend VREPJointDriver; //!< Friend class to access the data member

    /**
	 * @brief Start the communication with the simulator
	 * @param ip IP address to communicate with the simulator (default 127.0.0.1)
	 * @param port Port number used to communicate with the simulator (default 19997)
	 */
    void startCommunication(const std::string& ip, int port);

    static double vrep_cycle_time;     //!< Static variable holding the simulator control time step
    static int32_t client_id;          //!< Static variable used to access the client ID provided by the V-REP simulator
    static int sim_time_ms;            //!< Static variable giving the current simulation time
    static bool registered_in_factory; //!< Indicate if the driver has been registered in the factory

    std::chrono::high_resolution_clock::time_point last_control_update_; //!< Time point updated each time a new control step begins
};
} // namespace rkcl
