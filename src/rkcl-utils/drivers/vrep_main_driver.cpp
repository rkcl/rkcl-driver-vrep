/**
 * @file vrep_main_driver.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a V-REP main driver to manage the execution of tasks on simulation
 * @date 21-02-2020
 * License: CeCILL
 */
#include <rkcl/drivers/vrep_driver.h>
#include <rkcl/data/timer.h>

#include <vrep_driver.h>
#include <simConst.h>

#include <yaml-cpp/yaml.h>

#include <stdexcept>
#include <iostream>
#include <thread>

using namespace rkcl;

bool VREPMainDriver::registered_in_factory = DriverFactory::add<VREPMainDriver>("vrep_main");
int VREPMainDriver::sim_time_ms = 0;
double VREPMainDriver::vrep_cycle_time = 0;
int32_t VREPMainDriver::client_id = 0;
VREPMainDriver::SyncData VREPMainDriver::sync_data;
std::mutex VREPMainDriver::vrep_com_mtx;

VREPMainDriver::VREPMainDriver(
    double cycle_time,
    int port,
    const std::string& ip)
    : last_control_update_(std::chrono::high_resolution_clock::now())
{
    sync_data.stop = false;
    vrep_cycle_time = cycle_time;
    startCommunication(ip, port);
}

VREPMainDriver::VREPMainDriver(
    Robot& robot,
    const YAML::Node& configuration)
    : VREPMainDriver(
          configuration["cycle_time"].as<double>(0.005),
          configuration["port"].as<int>(19997),
          configuration["ip"].as<std::string>("127.0.0.1"))
{
}

VREPMainDriver::~VREPMainDriver()
{
    simxStopSimulation(client_id, simx_opmode_oneshot_wait);
    simxSynchronous(client_id, false);
    simxFinish(client_id);
}

int32_t VREPMainDriver::getClientID()
{
    return client_id;
}

bool VREPMainDriver::init(double timeout)
{
    return true;
}

void VREPMainDriver::startCommunication(const std::string& ip, int port)
{
    std::lock_guard<std::mutex> lock_vrep(vrep_com_mtx);
    client_id = simxStart((simxChar*)ip.c_str(), port, 0, 1, 10000, int(vrep_cycle_time * 1000));

    if (client_id != -1)
    {
        simxSynchronous(client_id, true);
    }
    else
    {
        simxFinish(client_id);
        throw std::runtime_error("VREPMainDriver::init: can't initialize the connection with V-REP");
    }
}

bool VREPMainDriver::start()
{
    std::lock_guard<std::mutex> lock_vrep(vrep_com_mtx);
    bool ok = simxStartSimulation(client_id, simx_opmode_oneshot_wait) == simx_return_ok;

    // Run 2 steps to call the main script and the children scripts
    ok &= (simxSynchronousTrigger(client_id) == simx_return_ok);
    ok &= (simxSynchronousTrigger(client_id) == simx_return_ok);

    sim_time_ms = 0;

    return ok;
}

bool VREPMainDriver::stop()
{
    // notifyStop();
    std::lock_guard<std::mutex> lock_vrep(vrep_com_mtx);
    bool ok = (simxSynchronous(client_id, false) == simx_return_ok);
    ok &= (simxStopSimulation(client_id, simx_opmode_oneshot_wait) == simx_return_ok);
    return ok;
}

bool VREPMainDriver::read()
{
    return true;
}

bool VREPMainDriver::send()
{
    return true;
}

bool VREPMainDriver::sync()
{
    waitNextCycle();
    bool ok;
    {
        std::lock_guard<std::mutex> lock_vrep(vrep_com_mtx);
        ok = (simxSynchronousTrigger(client_id) == simx_return_ok);

        /*
		 * It takes some time to have the buffer updated after a simulation step
		 * so we call a blocking function to synchronize the communication thread.
		 * This way, the updaters will always have fresh data.
         * Note: This drastically slows down the simulation...
		 */
        int ping_time;
        simxGetPingTime(client_id, &ping_time);
    }
    updateRealTimeFactor();
    sim_time_ms = simxGetLastCmdTime(client_id);
    notifyNewData();
    if (not ok)
        std::cout << "VREPMainDriver::sync NOT OKAY \n";
    return ok;
}

void VREPMainDriver::waitNextCycle()
{
    auto cycle = std::chrono::microseconds(static_cast<size_t>(vrep_cycle_time * 1e6));
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>((last_control_update_ + cycle) - std::chrono::high_resolution_clock::now());
    if (duration.count() > 0)
        std::this_thread::sleep_for(duration);
}

void VREPMainDriver::updateRealTimeFactor()
{
    Timer::realTimeFactor() = (vrep_cycle_time * 1e6) / std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - last_control_update_).count();
    last_control_update_ = std::chrono::high_resolution_clock::now();
}

void VREPMainDriver::notifyNewData()
{
    std::unique_lock<std::mutex> sync_lck(sync_data.mtx);
    for (auto notify_driver : sync_data.notify_drivers)
        notify_driver = true;
    sync_data.cv.notify_all();
}

void VREPMainDriver::notifyStop()
{
    std::unique_lock<std::mutex> sync_lck(sync_data.mtx);
    sync_data.stop = true;
    sync_data.cv.notify_all();
}
