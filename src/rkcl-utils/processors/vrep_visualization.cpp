/**
 * @file vrep_visualization.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Defines a simple utility class for visualization in V-REP
 * @date 21-02-2020
 * License: CeCILL
 */
#include <rkcl/processors/vrep_visualization.h>
#include <rkcl/drivers/vrep_main_driver.h>

#include <vrep_driver.h>
#include <simConst.h>

#include <iostream>

using namespace rkcl;

VREPVisualization::VREPVisualization(Robot& robot,
                                     CollisionAvoidancePtr collision_avoidance,
                                     WorldObjectsBehavior behavior)
    : robot_(robot),
      collision_avoidance_(collision_avoidance),
      world_objects_behavior_(behavior)
{
}

VREPVisualization::~VREPVisualization() = default;

void VREPVisualization::init()
{
    robot_collision_object_handles_.clear();
    world_collision_object_handles_.clear();
    observation_point_handles_.clear();
    world_collision_object_poses_.clear();

    if (collision_avoidance_)
    {
        for (size_t i = 0; i < collision_avoidance_->robotCollisionObjectCount(); ++i)
        {
            auto rco = collision_avoidance_->robotCollisionObject(i);
            int handle;
            if (simxGetObjectHandle(VREPMainDriver::getClientID(), rco->name().c_str(), &handle, simx_opmode_oneshot_wait) == simx_return_ok)
                robot_collision_object_handles_[rco->name()] = handle;
        }

        for (const auto& wco : collision_avoidance_->worldCollisionObjects())
        {
            int handle;
            if (simxGetObjectHandle(VREPMainDriver::getClientID(), wco->name().c_str(), &handle, simx_opmode_oneshot_wait) == simx_return_ok)
            {
                world_collision_object_handles_[wco->name()] = handle;
                if (world_objects_behavior_ == WorldObjectsBehavior::ReadPoseFromVREP)
                {
                    float f;
                    simxGetObjectPosition(VREPMainDriver::getClientID(), handle, -1, &f, simx_opmode_streaming);
                    simxGetObjectQuaternion(VREPMainDriver::getClientID(), handle, -1, &f, simx_opmode_streaming);
                }
            }
            else
            {
                std::cout << "Collision object " << wco->name() << " not found in the VREP scene: won't be displayed\n";
            }
        }
    }

    for (size_t i = 0; i < robot_.observationPointCount(); ++i)
    {
        auto op = robot_.observationPoint(i);
        int handle;
        if (simxGetObjectHandle(VREPMainDriver::getClientID(), op->name().c_str(), &handle, simx_opmode_oneshot_wait) == simx_return_ok)
            observation_point_handles_[op->name()] = handle;
    }

    for (size_t i = 0; i < robot_.controlPointCount(); ++i)
    {
        auto cp = robot_.controlPoint(i);
        int handle;
        if (simxGetObjectHandle(VREPMainDriver::getClientID(), cp->name().c_str(), &handle, simx_opmode_oneshot_wait) == simx_return_ok)
            observation_point_handles_[cp->name()] = handle;
    }
}

bool VREPVisualization::process()
{
    bool all_ok = true;
    // world_collision_object_poses_.clear();
    std::lock_guard<std::mutex> lock_vrep(VREPMainDriver::vrep_com_mtx);
    simxPauseCommunication(VREPMainDriver::getClientID(), 1);

    auto write_object_pose = [](const Eigen::Affine3d& pose, int handle)
    {
        bool ok = true;
        auto trans = pose.translation();
        float pos_data[3];
        for (size_t i = 0; i < 3; ++i)
            pos_data[i] = trans(i);
        ok &= simxSetObjectPosition(VREPMainDriver::getClientID(), handle, -1, pos_data, simx_opmode_oneshot) < simx_return_timeout_flag;
        Eigen::Quaterniond q(pose.rotation());
        double* q_data_d = q.coeffs().data();
        float q_data_f[4];
        for (size_t i = 0; i < 4; ++i)
            q_data_f[i] = q_data_d[i];
        ok &= simxSetObjectQuaternion(VREPMainDriver::getClientID(), handle, -1, q_data_f, simx_opmode_oneshot) < simx_return_timeout_flag;
        return ok;
    };

    for (const auto& [name, handle] : robot_collision_object_handles_)
    {
        auto rco = collision_avoidance_->robotCollisionObject(name);
        all_ok &= write_object_pose(rco->poseWorld(), handle);
    }

    for (const auto& [name, handle] : world_collision_object_handles_)
    {
        auto wco = collision_avoidance_->worldCollisionObject(name);
        if (world_objects_behavior_ == WorldObjectsBehavior::ReadPoseFromVREP)
        {
            float pos_data[3];
            all_ok &= simxGetObjectPosition(VREPMainDriver::getClientID(), handle, -1, pos_data, simx_opmode_buffer) < simx_return_timeout_flag;
            float q_data[4];
            all_ok &= simxGetObjectQuaternion(VREPMainDriver::getClientID(), handle, -1, q_data, simx_opmode_buffer) < simx_return_timeout_flag;
            Eigen::Affine3d new_pose;
            new_pose.translation() = Eigen::Vector3d(pos_data[0], pos_data[1], pos_data[2]);
            new_pose.linear() = Eigen::Quaterniond(q_data[3], q_data[0], q_data[1], q_data[2]).matrix();
            world_collision_object_poses_[wco->name()] = new_pose;
        }
        else
        {
            all_ok &= write_object_pose(wco->poseWorld(), handle);
        }
    }

    for (const auto& [name, handle] : observation_point_handles_)
    {
        auto op = robot_.observationPoint(name);
        all_ok &= write_object_pose(op->state().pose(), handle);
    }

    simxPauseCommunication(VREPMainDriver::getClientID(), 0);
    return all_ok;
}
