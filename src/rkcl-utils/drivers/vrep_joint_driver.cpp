/**
 * @file vrep_joint_driver.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a V-REP driver to control a specific joint group on simulation
 * @date 21-02-2020
 * License: CeCILL
 */
#include <rkcl/drivers/vrep_driver.h>

#include <vrep_driver.h>
#include <simConst.h>

#include <yaml-cpp/yaml.h>

#include <stdexcept>
#include <iostream>
#include <thread>

using namespace rkcl;

bool VREPJointDriver::registered_in_factory = DriverFactory::add<VREPJointDriver>("vrep_joint");
size_t VREPJointDriver::joint_driver_count = 0;

VREPJointDriver::VREPJointDriver(
    Robot& robot,
    const YAML::Node& configuration)
{
    configure(robot, configuration);
    driver_idx_ = joint_driver_count++;
    VREPMainDriver::sync_data.notify_drivers.resize(joint_driver_count);
}

const bool& VREPJointDriver::isActivated()
{
    return is_activated_;
}

void VREPJointDriver::configure(Robot& robot, const YAML::Node& configuration)
{
    std::vector<std::string> joint_names;
    try
    {
        joint_names = configuration["joint_names"].as<std::vector<std::string>>();
    }
    catch (...)
    {
        throw std::runtime_error("VREPJointDriver::VREPJointDriver: You must provide a 'joint_names' vector");
    }
    for (const auto& name : joint_names)
    {
        std::lock_guard<std::mutex> lock_vrep(VREPMainDriver::vrep_com_mtx);
        int handle;
        if (simxGetObjectHandle(VREPMainDriver::client_id, name.c_str(), &handle, simx_opmode_oneshot_wait) != simx_return_ok)
        {
            simxSynchronous(VREPMainDriver::client_id, false);
            simxFinish(VREPMainDriver::client_id);
            throw std::runtime_error("VREPJointDriver::VREPJointDriver: unable to retrieve handle for joint " + name);
        }
        joint_handles_.push_back(handle);

        float pos;
        simxGetJointPosition(VREPMainDriver::client_id, handle, &pos, simx_opmode_streaming);
    }

    std::string joint_group;
    try
    {
        joint_group = configuration["joint_group"].as<std::string>();
    }
    catch (...)
    {
        throw std::runtime_error("VREPJointDriver::VREPJointDriver: You must provide a 'joint_group' field");
    }
    joint_group_ = robot.jointGroup(joint_group);
    if (not joint_group_)
        throw std::runtime_error("VREPJointDriver::VREPJointDriver: unable to retrieve joint group " + joint_group);

    auto control_mode = configuration["control_mode"];
    if (control_mode)
    {
        auto control_mode_str = control_mode.as<std::string>();
        if (control_mode_str == "Position")
            control_mode_ = ControlMode::Position;
        else if (control_mode_str == "Velocity")
            control_mode_ = ControlMode::Velocity;
        else
            throw std::runtime_error("VREPJointDriver::VREPJointDriver: Invalid control mode, accepted entries are 'Position' or 'Velocity'");
    }

    auto is_dynamic = configuration["is_dynamic"];
    if (is_dynamic)
        is_dynamic_ = is_dynamic.as<bool>();
}

VREPJointDriver::~VREPJointDriver()
{
}

bool VREPJointDriver::init(double timeout)
{
    double waited_for = 0.;
    auto cycle = std::chrono::microseconds(static_cast<size_t>(VREPMainDriver::vrep_cycle_time * 1e6));

    while (not read() and waited_for < timeout)
    {
        std::this_thread::sleep_for(cycle);
        waited_for += VREPMainDriver::vrep_cycle_time;
    }

    bool ok = waited_for < timeout;
    if (not ok)
        std::cerr << "VREPJointDriver::init: timeout reached \n";
    reset();

    jointGroupLastStateUpdate() = std::chrono::high_resolution_clock::now();

    return ok;
}

void VREPJointDriver::reset()
{
    velocity_command_ = jointGroupState().velocity();
}

bool VREPJointDriver::start()
{
    return true;
}

bool VREPJointDriver::stop()
{
    bool all_ok = true;

    size_t idx = 0;
    std::lock_guard<std::mutex> lock_vrep(VREPMainDriver::vrep_com_mtx);
    simxPauseCommunication(VREPMainDriver::client_id, 1);
    if (control_mode_ == ControlMode::Position)
    {
        if (is_dynamic_)
            for (auto joint : joint_handles_)
            {
                all_ok &= simxSetJointTargetPosition(VREPMainDriver::client_id, joint, jointGroupState().position()(idx++), simx_opmode_oneshot) < simx_return_timeout_flag;
            }
        else
            for (auto joint : joint_handles_)
            {
                all_ok &= simxSetJointPosition(VREPMainDriver::client_id, joint, jointGroupState().position()(idx++), simx_opmode_oneshot) < simx_return_timeout_flag;
            }
    }
    else if (control_mode_ == ControlMode::Velocity)
        for (auto joint : joint_handles_)
        {
            all_ok &= simxSetJointTargetVelocity(VREPMainDriver::client_id, joint, 0, simx_opmode_oneshot) < simx_return_timeout_flag;
        }
    simxPauseCommunication(VREPMainDriver::client_id, 0);
    return all_ok;
}

bool VREPJointDriver::read()
{
    bool all_ok = true;
    if (is_activated_)
    {
        size_t idx = 0;
        Eigen::VectorXd pos_vec(jointGroup()->jointCount());
        std::lock_guard<std::mutex> lock_vrep(VREPMainDriver::vrep_com_mtx);
        for (auto joint : joint_handles_)
        {
            float pos;
            bool ok = simxGetJointPosition(VREPMainDriver::client_id, joint, &pos, simx_opmode_buffer) == simx_return_ok;
            if (ok)
            {
                pos_vec(idx) = pos;
            }
            ++idx;
            all_ok &= ok;
        }
        std::lock_guard<std::mutex> lock(joint_group_->state_mtx_);
        jointGroupState().position() = pos_vec;
        jointGroupLastStateUpdate() = std::chrono::high_resolution_clock::now();
    }

    return all_ok;
}

bool VREPJointDriver::send()
{
    bool all_ok = true;

    if (is_activated_)
    {
        std::lock_guard<std::mutex> lock(joint_group_->command_mtx_);
        velocity_command_ = joint_group_->command().velocity();
    }

    size_t idx = 0;
    std::lock_guard<std::mutex> lock_vrep(VREPMainDriver::vrep_com_mtx);
    simxPauseCommunication(VREPMainDriver::client_id, 1);
    if (control_mode_ == ControlMode::Position)
    {
        std::lock_guard<std::mutex> lock(joint_group_->command_mtx_);
        if (is_dynamic_)
            for (auto joint : joint_handles_)
            {
                all_ok &= simxSetJointTargetPosition(VREPMainDriver::client_id, joint, joint_group_->command().position()(idx++), simx_opmode_oneshot) < simx_return_timeout_flag;
            }
        else
            for (auto joint : joint_handles_)
            {
                all_ok &= simxSetJointPosition(VREPMainDriver::client_id, joint, joint_group_->command().position()(idx++), simx_opmode_oneshot) < simx_return_timeout_flag;
            }
    }
    else if (control_mode_ == ControlMode::Velocity)
        for (auto joint : joint_handles_)
        {
            all_ok &= simxSetJointTargetVelocity(VREPMainDriver::client_id, joint, velocity_command_(idx++), simx_opmode_oneshot) < simx_return_timeout_flag;
        }

    simxPauseCommunication(VREPMainDriver::client_id, 0);

    if (not all_ok)
        std::cerr << "VREPJointDriver::send: An error occurred while sending the data to VREP" << std::endl;

    if (is_activated_)
        last_control_time_ms_ = VREPMainDriver::sim_time_ms;

    std::lock_guard<std::mutex> lock(joint_group_->state_mtx_);
    jointGroupState().acceleration() = (velocity_command_ - jointGroupState().velocity()) / joint_group_->controlTimeStep();
    jointGroupState().velocity() = velocity_command_;

    return all_ok;
}

bool VREPJointDriver::sync()
{
    std::unique_lock<std::mutex> lock(VREPMainDriver::sync_data.mtx);

    VREPMainDriver::sync_data.cv.wait(
        lock,
        [this] {
            if (VREPMainDriver::sync_data.stop)
                return true;

            if (VREPMainDriver::sync_data.notify_drivers[driver_idx_])
            {
                VREPMainDriver::sync_data.notify_drivers[driver_idx_] = false;
                return true;
            }
            else
            {
                return false;
            }
        });

    if (VREPMainDriver::sim_time_ms - last_control_time_ms_ >= (int)(joint_group_->controlTimeStep() * 1e3))
        is_activated_ = true;
    else
        is_activated_ = false;

    return true;
}
