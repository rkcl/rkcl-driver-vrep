PID_Component(
    EXAMPLE_APPLICATION
    NAME simple-example
    DIRECTORY simple-example
    RUNTIME_RESOURCES examples_config examples_logs
    DEPEND
        rkcl-core/rkcl-core
        rkcl-core/rkcl-utils
        rkcl-fk-rbdyn/rkcl-fk-rbdyn
        rkcl-driver-vrep/rkcl-driver-vrep
        rkcl-otg-reflexxes/rkcl-otg-reflexxes
        pid-rpath/rpathlib
        rkcl-app-utility/rkcl-app-utility
        rkcl-osqp-solver/rkcl-osqp-solver
        pid-os-utilities/pid-signal-manager
)
